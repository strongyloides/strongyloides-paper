\documentclass[fleqn,10pt]{wlpeerj}
\usepackage{lineno}
\usepackage{graphicx}
%\usepackage[fleqn]{amsmath}

\title{A computer simulation model for Strongyloides infection}

\author[1]{Marcelo Levenhagen}
\author[2]{Gustavo Ferreira}
\author[2]{Getúlio Pereira}
\author[2]{Anderson Santos}
\affil[1]{Diagnosis of parasitosis Laboratory, Institute of Biomedical Sciences, Federal University of Uberlândia, Av. Para, 1720, 38400-902, Uberlandia, MG, Brazil}
\affil[2]{Faculty of Computing, Federal University of Uberlândia, Av. João Naves de Ávila, 2121, 38408-100, Uberlândia, MG, Brazil}
\affil[ ]{\footnotesize{Corresponding author:}}
\affil[ ]{\footnotesize{Anderson R. Santos\textsuperscript{2}}}
\affil[ ]{\footnotesize{Email address: santosardr@ufu.br}}

\keywords{deterministic, Ordinary Differential Equations, COPASI, time course simulation, helminth, autoinfection, ivermectin}

\begin{abstract}
Despite high success rate in treating human Strongyloides infection with ivermectin, there is no consensus about the treatment of hyperinfection by helminths of the genus Strongyloides in immunocompromised people. Chances of survival of patients suffering from strongyloidiasis hyperinfection near 50\%, with the toxicity of ivermectin in nervous system aggravating the puzzle. Creation of computer models capable of faithfully reproduce established knowledge about Strongyloides infection may be useful to understand the dynamics of infection and possible treatment. A computer model was developed in COPASI using ordinary differential equations representing dynamics of helminth life cycle in a hyperinfection. A combination of Boolean variables representing an ’immunosuppressed’ host and ’treated’ with four doses of ivermectin (200 $\mu$kg$^{-1}$) raises four scenarios for time course simulations. Humoral and cellular immune responses were modeled using previous experiments whose data were approximated by common functions. Three experiments, associated to strongyloidiasis hyperinfection, were successfully reproduced. A fourth scenario (immunosuppressed $ \wedge $ treated) yielded similar results compared to literature: four doses of ivermectin (200 $\mu$kg$^{-1}$) were sufficient to restrain an infection, without a need for prolonged treatment. However, the smallest dosage does not suffice. A computational model for Strongyloides infection is available in SBML file format. The computational model here proposed for simulation of a hyperinfection by Strongyloides achieved similar results to previous experiments with hosts. It could allow us an understanding of several aspects of infections caused by parasites of the genus Strongyloides.

\end{abstract}

\begin{document}

\makeatletter 
\renewcommand\@biblabel[1]{#1.} 
\makeatother

\flushbottom
\maketitle
\thispagestyle{empty}
\linenumbers %lineno.sty

\section{Introduction}
\label{intro}

Human Strongyloides infection is a parasitosis caused by a helminth of genus Strongyloids \citep{Blaxter1998}. Strongyloides infection is worldwide, affecting about 100 million people in the tropics and subtropical regions of Asia, Africa, and Latin America, especially in developing countries \citep{Buonfrate2015}. In such countries, precarious sanitary conditions plus high humidity, temperature, and soil favor the parasite development, resulting in high endemic regions \citep{Schar2013}. In these areas, the \textit{Strongyloides stercoralis} species is the principal etiological agent of human Strongyloides infection \citep{Requena-Mendez2013}.
\textit{S. stercoralis} species has two modes of infection: primary and autoinfection. Primary infection consists of a parasite infecting form, the filariform larvae, invasion through the skin or mucosa of the host organism. Filariform larvae perform the Loss cycle after swallowed to reach the small intestine. Larvae transform into female and initiate parthenogenetic oviposit. After eggs hatch in the mucosa occurs the releasing of rhabiditiform larvae which can be haploid (1n), diploid (2n) or triploid (3n)\citep{Costa-Cruz2011}. In the external environment, larvae develop a direct or an indirect cycle. In the reporting period, rhabiditiform larvae transform to filariform. In the indirect cycle diploid and haploid rhabiditiform larvae emerge to female and male, respectively. Larvae mating results in eggs, larvae rhabditoid and then the filariform larvae.
The prepatent period in humans ranges from 8 to 25 days \citep{Costa-Cruz2011}. In the autoinfection, rhabiditiform larvae change to filariform larvae in the perianal region or intestinal mucosa. Individuals with helminth infection are asymptomatic in most cases, with reports of abdominal pain. Many symptoms present themselves in the mild form, similar to other parasitic infections \citep{Agrawal2009}. Another important species is \textit{Strongyloides venezuelensis}, often used for better understanding of murine models infection. \textit{S. venezuelensis} and \textit{S. stercoralis} develop in the environment and hosts organisms. The principal distinction in the life cycle of these two species during infection is that in \textit{S. venezuelensis} eggs shed through feces but \textit{S. stercoralis} larvae \citep{Yasuda2014}.
Strongyloidiasis autoinfection is responsible for increasing the patient parasitic load and stretched permanence of the parasite in the host organism. Autoinfection is also responsible for developing the severe form of the disease known as hyperinfection, leading to the spread of the parasite to other organs \citep{Paula2011}. Immunocompromised individuals, especially those using immunosuppressants, could be under severe health conditions caused by hyperinfection due to parasite dissemination through the body \cite{Buonfrate2013}. A rising number of excreted larvae via stool or sputum characterizes hyperinfection in a host. Hyperinfection causes gastrointestinal and respiratory symptoms, where the parasitic cycle develops \citep{De_Souza2014}.
Higher mortality rates occur when the parasite is widespread in organs such as liver, kidney or brain \citep{Stewart2011}. Treatment of human Strongyloides infection consists of the oral or subcutaneous administration of ivermectin, a veterinary drug. However, there is no agreement regarding the suitable dosage, frequency or administration route of the drug. Comparing different results of surveys on these parameters confirms the lack of consensus on dosages. Also, results regarding healing and survival of patients under equal treatment evidence nonstandard treatment \citep{Pacanowski2005,Lichtenberger2009,Turner2005,Grein2010,Chiodini2000}.
There is a little description in the literature documenting immune response stimulated by the parasite, presumably because of a complex nature of the host-parasite interaction. In general, in the immune response to helminths, cellular and humoral responses take part. There is the production of specific signaling pathway interleukins such as IL-3, IL-4, IL5 and IL-13 with helminth antigens presented by MHC class II to the CD4, Th2 profile \citep{Rodrigues2013}. Th2 cytokine profile elicits a humoral response mediated by IgE and IgA, additionally cell-mediated response mediated by eosinophils, basophils, and mast cells. These triggered processes are essential in eliminating worms and larvae of the infected organism \citep{MARCOS2011}. IgA allows mucosal protection and IgE promote a faster immune response effector against parasites \citep{Galioto2006}. Immunocompetent patients with Strongyloides infection have elevated levels of IgE. In patients with immunosuppression, these levels are within normal limits \citep{Costa-Cruz2011}. The T-independent mechanism also acts in the immune response against the parasite. Cytokines produced in this pathway as IL-1 and tumor necrosis factor (TNF-alpha), stimulate goblet glands gut to produce mucus, preventing the establishment of female larvae parthenogenetic in the mucosa and promoting its elimination in feces \citep{Machado2011}. Antigens of the larvae parasite activate classical and alternatives pathways of the complement system, which allows adherence of monocytes and polymorphonuclear cells to the surface of these larvae, promoting their lysis. One of the parasite’s evasions strategies is the protein cleavage of the complement system \citep{Costa-Cruz2011}.
Strongyloides infection diagnosis should be made as quickly as possible to allow preventing the disease development to dissemination and hyperinfection.
There are three methods of diagnosis: parasitologic, molecular and immunological \citep{Levenhagen2014}. Strongyloides infection treatment disposes of some antiparasitic drugs such as Ivermectin, Thiabendazole, Mebendazole, and Albendazole. Albendazole acts on different larval forms of the parasite decreasing the reproduction and survival of the parasite, preventing it from capture glucose from the host organism together with glycogen depletion and decreasing ATP production \citep{DeBona2008}. Thiabendazole acts only in parthenogenetic female inhibiting its metabolism but is not effective in eliminating the disease in patients with hyperinfection \citep{Costa-Cruz2011}. Ivermectin is very effective because it has an antilarval activity to induce tonic paralysis in the muscles of the parasite \citep{Amato_Neto1997}. These drugs can present some toxicity for a patient organism, and some can lead to the development of larvae resistance. Such consequences face a significant challenge in their use \citep{Goossens2015}.
Since using immunosuppressive drugs is common and efficient practice in curing various diseases \citep{Machado2011}, is acceptable to consider a prevalent use of these substances. However, if a patient suffers from Strongyloides infection and fits for immunocompromised by toxic elements or undergoing an immunosuppressive disease process, this one assumes a risk of developing an hyperinfection. In these case, there is often the death of this patient, even when applying treatments that had healing success or survival in other patients \citep{Donadello2013}. We examine the hypothesis of computationally reproducing a hyperinfection process by Strongyloides, based on results available in the literature about this parasite, under an animal model \citep{Machado2011,Goncalves2010,de_Melo2012}. Moreover, a simulated treatment for hyperinfection was studied, by several ivermectin doses, according to results presented in the literature \citep{Donadello2013}.

\section{Materials and Methods}
\label{sec:1}
\subsection{Simulation techniques}
\label{sec:2}

There are two main theories for computer simulation of real world events: Reductionist and Holistic. The Reductionist fragments a problem into smaller parts for better understanding and analysis, whereas for Holistic theory a problem, as a whole, gives clues about how their parties should behave \citep{Fang2011}. For instance, the reductionist approach uses agent-based and cellular automata methods. Agent-based encompasses autonomous and unrestricted software interaction guided by behavior rules and memory store. Cellular automata approach assumes a hypothetical infinite cell number arranged in a regular mesh and finite automata number locally communicating by a deterministic, uniform and synchronized way. The main differences between cellular automata and agent-based approach are memoryless and an interaction limited number of the former \citep{Figueredo2012}. Monte Carlo, Dynamical Systems (DS) and Ordinary Differential Equations (ODEs) are traditional approaches from Holistic theory. Monte Carlo method is used commonly for modeling biological problems, random numbers generating and numerical solutions. However, this proposal does not allow to analyze a system dynamics, focusing solely on the final result. The System Dynamics is a strategy based on interdependence, mutual interaction, information feedback and circular causality. SD is appropriate for dynamic simulations with actions, flows and feedback cycles. Another approach is System Dynamic Simulations (SDS), a continued simulation for an SD model. SDS is solved integrating an ODE set for a particular time interval. An approach based solely on ODEs is adopted commonly in problems with simple dynamic, few variables and with no distinction between modeled entities of a class \citep{Figueredo2012}. The present study is based on a Holistic ODE modeling approach because there are few items (also named species in the COPASI tool) and variables in a model representing a small number of controlled situations.

\subsection{COPASI tool}
\label{sec:3}
In this study, COPASI was used to create the model, quantity estimation of kinetic parameters, the initial concentration of modeled entities present in a strongyloidiasis hyperinfection and the simulation test cases addressed. COPASI is a software for simulation and analysis of biochemical networks and their dynamics. Supports SBML standard models and allows to simulate their behavior by deterministic or stochastic simulation (Gillespie algorithm). Also, simulations include arbitrary discrete events. The software likewise permits to explore, for instance, parameter estimating and optimization, besides functionalities to data visualization, histograms and diagram animations \citep{Hoops2006}.
\subsection{Scenarios}
\label{sec:4}
The scenarios here explored considered as a start point the existence of just one adult worm with full reproductive capability. A combination of variables health (immunocompromised or immunocompetent) and treatment (ivermectin use or not) expressed the differences between scenarios according to the Table 1.

\begin{table}[h!]
\caption{Simulated scenarios.}
	\begin{tabular}{lllll}
	\hline 
	\textbf{Scenario} & \textbf{Health} & \textbf{Treatment} & \textbf{Description} & \textbf{\parbox[t]{2.3cm}{Setting species\\of the model\\ to simulate it\\}}\\ 
	\hline 
	S1 & immunocompetent & water & \parbox[t]{1.5cm}{healthy and\\ not treated} & \parbox[t]{2.3cm}{dexa=0,\\worm=1,\\ivermectin=0\\} \\ 
	\hline 
	S2 & immunocompetent & ivermectin & \parbox[t]{1.5cm}{healthy and\\treated}  & \parbox[t]{2.3cm}{dexa=0,\\worm=100,\\ivermectin=200\\} \\ 
	\hline 
	S3 & immunocompromised & water & \parbox[t]{1.5cm}{unhealthy and\\not treated}  & \parbox[t]{2.3cm}{dexa=1,\\worm=1,\\ivermectin=0\\} \\ 
	\hline 
	S4 & immunocompromised & ivermectin & \parbox[t]{1.5cm}{unhealthy and\\treated}  & \parbox[t]{2.3cm}{dexa=1,\\worm=100,\\ivermectin=200\\} \\ 
	\hline 
	\end{tabular} 
\end{table}

\subsection{Model}
\label{sec:5}
Figure 1 depicts the modeled species in this study and some interactions’ names of importance. An ellipsoidal symbol was adopted to model nocive entities to the host as the worm, egg, larvae and dexamethasone (DEXA). Similarly but counter-clock, a rectangular representation was chosen for not nocive entities to the host like ivermectin, humoral response (HR) and cellular response (CR), supposing these will guarantee health establishment in the host. An arrow connection determines an entity influence over other, just like a directed graph.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{/home/anderson/Dropbox/Papers/Strong/pardis/submission/Figure1.eps}
  \caption{ Interaction diagram of species, their associated constants and variables.}
\end{figure}

Constants, formulas or ODEs, controlling the power levels between connected species, gives the arrow names of the model. We opt to conjugate Immune responses within the single species HR and CR. Immunoglobulins and leukocytes levels were used to model HR and CR responses. We compute an immune response negatively influencing the infection by summing up its implicitly modeled subentities. The so-called immunological subspecies of the model were not implemented by ODEs but by curve fitting using logistic, logarithmic and exponential functions to referenced data produced in no human hosts \citep{Machado2011, Goncalves2010}. DEXA represents the immunosuppressive species of the model. It positively influences egg formation and eclosion liberating larvae in the host \citep{Donadello2013}. DEXA also negatively regulates immune responses, being responsible for a reduced CR against the Strongyloides infection \citep{Machado2011}. By turn, CR negatively influences the parasite promoting its eradication by larvae lysis having as players monocytes and polymorphonuclear cells \citep{Costa-Cruz2011}.
In figure 1 was modeled a negative CR influence over larvae and eggs. Also, we did not model DEXA influence over HR but to allows HR oscillate within larger and lesser activity periods starting since the primary infection and persisting through time \cite{Murphy2012}. The ivermectin drug (IVER), a well standard for Strongyloides infection, was modeled exercising negative influence over the worm leading it to death \citep{Donadello2013}. The Influence of DEXA over IVER was assumed positive nurtured by one literature record of strengthened drug retention for an immunosuppressed patient \citep{van_Westerloo2014}. The IVER species participates of the scenarios S2 and S4. Therefore, we implemented a formula regulated by the $K\_death\_iver$ constant to keep the IVER species in the host for small periods of time, like reported to immunocompetent (S2) or prolonged time (S4).

\subsection{Model developing}
\label{sec:6}
ODEs were created based on the diagram from the Figure 1, a single compartment diagram and with species measured as concentrations. Functions intended to variable’s settings, accordingly to referenced data, were omitted. For clearness purposes of the model, mathematical functions designed to variable’s settings, accordingly to referenced data, were omitted. However, these can be found in the published SBML model (Additional file 1)

\begin{align}
\frac {\mathrm{d}\left( {{\mathrm{[worm]}} \, } \right) } {\mathrm{d}{t} } = \,   { {{\mathrm{kw\_gen}} \, {\mathrm{[larvae]}} } - { \mathrm{[treatment]} \, {\mathrm{[worm]}} } }  - {{\mathrm{kw\_death}} \, {\mathrm{[worm]}} }   
\end{align}

\begin{align}
\frac {\mathrm{d}\left( {{\mathrm{[egg]}} \, } \right) } {\mathrm{d}{t} } &= \, { {{\mathrm{ke\_gen}} \, {\mathrm{[worm]}} } - { \mathrm{[treatment]} \, {\mathrm{[egg]}} } } \nonumber \\ & - {{\mathrm{[cellular]}}^ {{\mathrm{kc\_power}} } \, {\mathrm{[egg]}} }  -  {{\mathrm{[humoral]}}^ {{\mathrm{kh\_power}} } \, {\mathrm{[egg]}} }  
\end{align}

\begin{align}
\frac {\mathrm{d}\left( {{\mathrm{[larvae]}} \, } \right) } {\mathrm{d}{t} } &= \,  \mathrm{kl\_gen} \, {\mathrm{[egg]}}  - { \mathrm{[treatment]} \, {\mathrm{[larvae]}} } \nonumber \\ &- {{\mathrm{[cellular]}}^ \mathrm{kc\_power}  \, {\mathrm{[larvae]}} }  - \left( {\mathrm{[humoral]}} \, {\mathrm{[larvae]}}  \right)^ {\mathrm{kh\_power}}  
\end{align}

\begin{align}
\frac {\mathrm{d}\left( {{\mathrm{[ivermectin]}} \, } \right) } {\mathrm{d}{t} } = \, { {{\mathrm{[ivermectin]}} \, {\mathrm{kiver\_decay}} } }  
\end{align}

\begin{align}
{\mathrm{treatment}} = { {\mathrm{kw\_death\_iver}} \, {{\mathrm{[ivermectin]}} \, {\mathrm{kiver\_decay}} } }  
\end{align}

Equations 1-3 has the calling of equation 5, which take part wherever the host is under treatment for the infection or else when the ivermectin dose is greater than zero. In Equation 5, a worm mortality constant $kw\_death\_iver$ IVER is conducive to diminish the worm and larvae concentrations in the case of an ivermectin treatment. However, the diminishing concentrations process is softened by a natural ivermectin concentration decay in the host symbolized by the $kiver\_decay$ constant decline. It was assumed higher values for the $kiver\_decay$ constant in the case of immunosuppression when compared to immunocompetence situation \citep{van_Westerloo2014}. The equation 1 represents the adult worm concentration positively depending on the larvae concentration and negatively of a possible infection treatment. Equation 1 also is adversely affected by the parasite natural mortality death, represented by the $kw\_death$ constant. For immunocompetent cases, the $kw\_death$ constant was empirically assigned as 50\% but reduced by 32\% due to data fitting if immunosuppression takes place. Also, we empirically kept $kw\_gen$ and $kl\_gen$ constants as 100\% representing worm and larvae generation rates, respectively. Equation 2 differs from 1 because of the absence of an eggs decay constant. In equation 2, cellular and humoral immune responses are responsible for diminishing eggs concentration. The average number of eggs deposited by an adult worm (40), fully capable of reproducing, set the $kw\_gen$ constant. It worth to emphasize that concentrations of immune system agents read from literature experimental data were measured using Optical Density (OD). These ODs has a measurement scale orders of magnitude smaller than antigens concentrations.
OD values were undergone power law distributions to raise immune species concentrations to significant levels capable of to deal antigens concentration.
Model fit the referenced data rendered a value of 0,57 to the $kc\_power$ constant. Concerning adaptive immune responses, the OD of IgE immunoglobulin tends to rise according to the infection escalation. It happens even for immunosuppressed hosts but in a smaller baseline than for immunocompetent hosts \citep{Machado2011}. According to this IgE raising tendency in Strongyloides infection, the $kh\_power$ is a compound function including different exponential functions acting on specific infection periods. Equation 3 (larvae generation rate) differs from 2 (eggs production rate) by the adaptive immune system strength to restrain larvae. In the Equation 3, suppressing of larvae occurs in a higher baseline compared to eggs since there is an IgE OD value multiplication by larvae concentration before $kh\_power$ affect it. Such calculations to restrain larvae in a more accentuated way than eggs bases on an established knowledge about hatching eggs to larvae in the intestinal mucosa, for instance, concerning \textit{S. stercoralis}. Finally, equation 4 considers an ivermectin decay rate in the host whenever is being simulated a treatment condition with ivermectin greater than zero. Treatment simulations comprised four ivermectin doses of 200 $\mu$kg$^{-1}$, each 48 hours.

\section{Results and Discussion}
\label{sec:7}

\subsection{Scenarios}
\label{sec:8}
For the scenarios results, it was adopted as standard the infection initiation consisting of just one adult worm with full reproduction capabilities. Days of infection and studied species concentrations are represented in the x-axis and y-axis, respectively.

\subsubsection{Scenario S1 - immunocompetent and untreated host}
\label{sec:9}

Figure 2 depicts a simulation result of Strongyloides infection in a host not immunosuppressed and without drug treatment. The curve represents the eggs per gram of feces. At the eighth day, fitting referenced data, we enter the higher concentration of eggs. From that on, we obtain a significantly diminishing of eggs frequency. Such decreasing culminated in the disruption of the infection from the twenty-first day \citep{Machado2011,Goncalves2010}. Referenced data showed eggs measured by thousands without a thin measurement after the twenty-first day. The gross measure of eggs and larvae is a limitation of current diagnostic parasitological methods \citep{Schar2014}, an obstacle less hard to transpose using computational methods. Results here presented supports a significant infection softness after the twenty-first day but no disruption. Instead infection disruption, there would be a few hundred of eggs per gram of feces, a few dozens after fortieth day and infection disruption after the seventieth-eight day.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.25]{/home/anderson/Dropbox/Papers/Strong/pardis/submission/Figure2.eps}
  \caption{ Eggs curve for an infected host under conditions: immunocompetent and without treatment.}
\end{figure}

\subsubsection{Scenario S2 - immunocompetent and treated host}
\label{sec:10}
For this simulation, we assumed an empirical number of 100 adult worms initially infecting a host (Figure 3). Panel A is showing ivermectin dose peaks meaning drug administration each 48 hours. It also can be seen in panels B and C but at large scale. In this hypothetical scenario, an infection is disrupted after four 200 $\mu$kg$^{-1}$ doses of ivermectin (Panel B), administered each 48 hours, as reported in experiments with human beings \citep{Donadello2013}. An additional simulation was made decreasing the ivermectin dose to 100 $\mu$kg$^{-1}$ (Panel C). With a reduced ivermectin dosage, compared to standard dosage, there is larvae reduction in significant levels but the infection perseveres after treatment, going a considerable number of eggs in the host.

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{/home/anderson/Dropbox/Papers/Strong/pardis/submission/Figure3.eps}
  \caption{ Eggs (red), larvae (magenta) and drug (blue dashed) curves for an infected host under conditions: immunocompetent and treated. Panel A points four ivermectin doses administered each 48 hours. It can be seen a drug concentration fast decaying through time, just like described in the literature concerning immunocompetent hosts. The drug curve also appears in panels B and C. Panel B shows an effective drug dosage applied causing the infection disruption in opposite to panel C where a smaller drug dosage took place.}
\end{figure}

\subsubsection{Scenario S3 - immunosuppressed  and untreated host}
\label{sec:11}
In Figure 4 is shown the simulation result of scenario S3. Similarly to S1, the infection spreading is fast, reaching a peak on the eighth day. However, now at a greater baseline \citep{Machado2011}. The immune system of the host can deal well and significantly reduce the infection after the twenty-first day, even immunosuppressed. The difference from S3 to S1 is that the infection is not disrupted after the twenty-first day but keep forward on higher baselines \citep{Machado2011}. The disease continues beyond the fortieth day in alternating cycles of highs and lows expelled eggs concentrations but without infection eradication \citep{de_Melo2012}.

\begin{figure}[h!]
\centering
\includegraphics[scale=1.5]{/home/anderson/Dropbox/Papers/Strong/pardis/submission/Figure4.eps}
  \caption{ For a host immunosuppressed and untreated, the infection is lessened in the first days. However, undergoes periodic waves of high and low eggs concentrations according to the adaptive immunological response to the infectious agent.}
\end{figure}


\subsubsection{Scenario S4 - immunosuppressed  and treated host}
\label{sec:12}
In Figure 5, we verified scenarios S2 and S3  concomitantly. We considered an initial load of 100 adult worms. In Panel A, ivermectin concentration administered four times through eight days was 200 $\mu$kg$^{-1}$. Under such conditions, the concentration of eggs has diminished after the eighth day. In Panel B, the ivermectin concentration was reduced to 100 $\mu$kg$^{-1}$ compared to panel A. The model shows the eggs level significantly declined, although it did not suffice to squelch the infection.

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{/home/anderson/Dropbox/Papers/Strong/pardis/submission/Figure5.eps}
  \caption{ Ivermectin simulated treatment of a host suffering from Strongyloides infection using 200 and 100 $\mu$kg$^{-1}$ in panels A and B, respectively.}
\end{figure}    

\subsection{Animal versus human model}
\label{sec:13}
Experimental data of strongyloidiasis hyperinfection affecting people are scarce. Such limited data about human Strongyloides infection encouraged the creation of a simulation model based on a few human report cases and experiments in different hosts. These cases arising from various hosts and experimental conditions could grow doubts to the model proposed here. However, despite differences, there are similarities among these experiments. Within the first 21 days of trials, there is a standard curve shape concerning expelled eggs by different hosts. Also, should be noted, these first days are of crucial importance to those infection courses \citep{Machado2011,Goncalves2010,de_Melo2012}. Another interesting point, immunocompetent human hosts cannot get rid of Strongyloides infection without treatment, leading to chronic infection. Taking the similarities among referenced data here used as references, it sounds reasonable to use them to produce a general simulation model for Strongyloides infection.

\subsection{Immune system control over larvae}
\label{sec:14}
The model proposed here tried to summarize the maximization expectations according to a worm infection perspective against it antagonistic, the host immune system. Once reached an equilibrium in this struggle, one can simulate a chronic disease, just like proposed by the scenario S4 (Table 1). In the proposed model from this study, the capacity to restrain an infection is more robust for larvae than eggs. Adversely, in the case of larvae contention by the immune system, HR and CR are raised to their respective powers after final expression assessment. This difference of calculating the antigens contention level between eggs and larvae was proved essential to sound characterization of the model curves accordingly to referenced data. For instance, without this differentiation in the calculus, the model would present an extreme exponential rise to the larvae number in a few days and higher value to the eggs number. Literature can justify the larvae contention method we applied here about \textit{S. stercoralis}. For this helminth species, eggs amounts found in hosts are much smaller than larvae amount because of the short period necessary for eggs hatch \citep{Carvalho2015}, a fact capable of lead greater larvae exposure to the immune system and consequently enhancing facility to their identification. In this study,  we use referenced data coming from the \textit{S. venezuelensis} species, which it is known commonly for expelling more eggs than larvae through host feces. The eggs expelling assumption instead larvae could lead to the conclusion of reproducing similarities between \textit{S. venezuelensis} and \textit{S. stercoralis}.

\subsection{Autoinfection ruled by IgE levels}
\label{sec:15}
Autoinfection extends helminth presence in an immunocompromised host \citep{Agrawal2009,Costa-Cruz2011}. Strongyloides autoinfection in hosts comprises an extensively studied area. Also, in human beings, autoinfection occurs. Our model faithfully reproduced response levels of adaptive and innate immune system in light of 37 former infection days \citep{Machado2011,Goncalves2010,de_Melo2012}. For instance, the referenced data shows an HR response level (IgE) rising till a higher OD value \citep{Goncalves2010}. According to the model proposed here, in keeping IgE at higher levels for an undefined period then an infection would be nurtured for no more than 25 days (data not showed). However, referenced data shows infection in immunocompromised hosts lasting for much longer periods but at a weakened intensity. It was possible to reproduce such lasting and weakened disease from referenced data creating a sinusoidal function to promote increases and decreases of IgE level for a 25 days period. This precise strategy allows for simulating a periodic autoinfection in a host, being possible to produce a gradual reduction of the infection level with each following peak smaller but still chronical. The literature reports Strongyloides autoinfection composed of subsequently weakened peaks of eggs, similarly to waves \citep{de_Melo2012}. It was also reported IgE levels oscillating in an adaptive immunological response \citep{Murphy2012}.

\subsection{Experiment simulation and prognosis}
\label{sec:16}
In this study, data from the literature were used to test the model proposed here for scenarios S1-S3, but there were not enough data concerning S4. Except one report case regarding accentuated ivermectin retention by an immunosuppressed organism \citep{van_Westerloo2014}. This ivermectin emphasized retention take part of the model resulting in a slower ivermectin decay for S4 compared to other scenarios. As long as the model proposed here was capable of faithfully to reproduce the majority of test scenarios, it is not reasonable to discredit S4 predictions suggesting a possibility of disrupting a Strongyloides infection using a smaller ivermectin dosage at the cost of more time to eradication.

\section{Conclusion}
\label{sec:17}
By data aggregating and interleaving, from different experiments in the literature, benefited from similarities in the infection process, allowed the creation of the model here present. Scenarios of infections caused by organisms of the genus Strongyloides, common to different species, can be simulated using this model. The SBML model here presented can be one of the firsts in the battle against helminths of the genus Strongyloides, moving the battlefield against this parasite from the wet laboratory to the virtual environment.

\section{Additional Files}
\label{sec:20}
  \subsection{Additional file 1 --- An SBML file format for Strongyloides infection}
\label{sec:21}
    The SBML file was generated by the COPASI tool but can be managed by any SBML level 2 compliant program. An SBML (Strongyloides.xml) and a native COPASI file (Strongyloides.cps) formats are within this compressed file, both implementing the same model. There is also a library of piecewise defined functions (worm.cpk) to reproduce concentrations of immune system sub-entities.

\clearpage
\bibliography{strong}   % name your BibTeX data base

\end{document}
